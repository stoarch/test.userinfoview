package com.example;

import android.app.Application;

import com.example.api.JsonPlaceholderUsersApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Main application, that receive list of users and show it
 *
 * Created by stoarch on 8/4/2017.
 */
public class App extends Application {
    private static JsonPlaceholderUsersApi usersApi;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder().
                baseUrl("https://jsonplaceholder.typicode.com").
                addConverterFactory(GsonConverterFactory.create()).
                build();

        usersApi = retrofit.create(JsonPlaceholderUsersApi.class);
    }

    public static JsonPlaceholderUsersApi getApi(){
        return usersApi;
    }
}
