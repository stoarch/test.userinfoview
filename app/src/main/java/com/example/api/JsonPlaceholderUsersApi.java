package com.example.api;

import com.example.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Return users from service as list
 *
 * Created by stoarch on 8/4/2017.
 */
public interface JsonPlaceholderUsersApi {
    @GET("/users")
    Call<List<User>> getData();
}
