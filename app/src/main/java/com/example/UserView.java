package com.example;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.stoarch.userinfoviewer.MainActivity;
import com.example.stoarch.userinfoviewer.R;

import org.w3c.dom.Text;

public class UserView extends AppCompatActivity {

    private User user;
    private TextView idTextView;
    private TextView nameTextView;
    private TextView userNameTextView;
    private TextView emailTextView;
    private TextView companyTextView;
    private TextView phoneTextView;
    private TextView cityTextView;
    private TextView streetTextView;
    private TextView zipCodeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        idTextView = (TextView)findViewById(R.id.id_text_view);
        nameTextView = (TextView)findViewById(R.id.user_name_text_view);
        userNameTextView = (TextView)findViewById(R.id.user_name_text_view);
        emailTextView = (TextView)findViewById(R.id.email_text_view);
        companyTextView = (TextView)findViewById(R.id.company_text_view);
        phoneTextView = (TextView)findViewById(R.id.phone_text_view);
        cityTextView = (TextView)findViewById(R.id.city_text_view);
        streetTextView = (TextView)findViewById(R.id.street_text_view);
        zipCodeTextView = (TextView)findViewById(R.id.zip_code_text_view);

        Bundle b = this.getIntent().getExtras();
        if(b != null){
            user = (User)b.getSerializable(Constants.USER);
            updateUserView();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateUserView() {
        idTextView.setText(user.getId().toString());
        nameTextView.setText(user.getName());
        userNameTextView.setText(user.getUsername());
        emailTextView.setText(user.getEmail());
        companyTextView.setText(user.getCompany().getName());
        Address address = user.getAddress();
        cityTextView.setText(address.getCity());
    }

}
